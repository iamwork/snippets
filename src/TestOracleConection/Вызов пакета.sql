DECLARE
  l_array PG_FLOAT_COLLECTION.float_collection;
BEGIN
  l_array(0) := 100;
  l_array(1) := 200;  

  PG_FLOAT_COLLECTION.test_collection(l_array);
END;
