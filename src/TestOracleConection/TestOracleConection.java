package com.sender.processing.tests;

import oracle.jdbc.internal.OracleCallableStatement;
import oracle.jdbc.internal.OracleTypes;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;


public class TestOracleConection {
    public static void main(String[] args) throws Exception {

        // Connection conn = null;
            /*
            DriverManager.registerDriver(new oracle.jdbc.OracleDriver());

            conn = DriverManager.getConnection(
                    "jdbc:oracle:thin:@localhost:1521:orcl", "c##scott", "tiger"
            );
         */

        try (Connection conn = DriverManager.getConnection(
                "jdbc:oracle:thin:@localhost:1521:orcl", "c##scott", "tiger")) {

            if (conn != null) {

                System.out.println("Connected to the database!");

                OracleCallableStatement cstmt = null;

                cstmt = (OracleCallableStatement) conn.prepareCall
                        ("begin TEST_ALL.get_return_by_param (?,?); end;");

                cstmt.registerOutParameter(1, OracleTypes.CURSOR);
                cstmt.setInt(2, 16);

                cstmt.execute();
                ResultSet rs = (ResultSet) cstmt.getObject(1);

                while (rs.next()) {

                    System.out.println("L1 = " + rs.getString("L1") + " / " +
                            "H1 = " + rs.getString("H1"));
                }

                cstmt.close();

            } else {
                System.out.println("Failed to make connection!");
            }

        } catch (SQLException ex) {
            System.out.println("exception:  " + ex.getMessage());
            // if (conn != null) conn.close();
        }
    }

}