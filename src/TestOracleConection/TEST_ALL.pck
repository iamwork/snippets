CREATE OR REPLACE PACKAGE TEST_ALL AS


TYPE arr_type
             IS TABLE OF BINARY_FLOAT
                         INDEX BY BINARY_INTEGER;
                         
                         
type t_ref_cursor is ref cursor;  


PROCEDURE Test_1;  


PROCEDURE get_return_by_param(
    p_cursor       in out t_ref_cursor
   ,p_param in     data_time.eproch%type
  );

END TEST_ALL;
/
CREATE OR REPLACE PACKAGE BODY TEST_ALL AS

  PROCEDURE Test_1 AS
  
  a binary_integer;
  arr arr_type; 

  BEGIN
    
  arr(1) := 1;
  arr(2) := 2;
  arr(3) := 3;
  
  a := arr.first;
  
  dbms_output.put_line('TEST_1 = ' || arr(a));
  
  --CREATE_TABLE_DATA.Out_Screen(arr);
 -- NULL; 

 INSERT INTO DATA_TIME 
(fstprice, endprice, maxprice, minprice, price, l1, h1, mbid, mask, ask, bid, ord, hours, minuts, eproch) VALUES
(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 21);

COMMIT;
 
END Test_1;
  
  
  
  

PROCEDURE get_return_by_param(
    p_cursor       in out t_ref_cursor
   ,p_param in     data_time.eproch%type
  ) AS

  BEGIN
  
    open p_cursor for
      select   *
      from      DATA_TIME c
      where     c.EPROCH  = p_param 
    ;
    

  END get_return_by_param;
  
  
  

END TEST_ALL;
/
