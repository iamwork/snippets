CREATE OR REPLACE PACKAGE CREATE_TABLE_DATA AS

TYPE arr_type
             IS TABLE OF BINARY_FLOAT
                         INDEX BY BINARY_INTEGER;
                         
                         
PROCEDURE Out_Screen(arr arr_type);



END CREATE_TABLE_DATA;
/
CREATE OR REPLACE PACKAGE BODY CREATE_TABLE_DATA AS


PROCEDURE Out_Screen(arr arr_type)
IS
--index by binary_integer;
a binary_integer;


BEGIN

  --arr(1) := 1;
  --arr(2) := 2;
 -- arr(3) := 3;
  
  a := arr.first;
  dbms_output.put_line('Element = ' || arr(a));
  
  
END Out_Screen;





END CREATE_TABLE_DATA;
/
