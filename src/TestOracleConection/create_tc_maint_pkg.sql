CREATE OR REPLACE 
PACKAGE topcoder.tc_maint_pkg is

  /* ======================================================================== */
  /* type declarations                                                        */
  /* ======================================================================== */
  type t_ref_cursor is ref cursor;

  /* ======================================================================== */
  /* procedure/function declarations                                          */
  /* ======================================================================== */
  procedure insert_coder_rec(
    p_coder_id        in topcoder.coder.coder_id%type
   ,p_handle          in topcoder.coder.handle%type
   ,p_country_name    in topcoder.coder.country_name%type
   ,p_alg_rating      in topcoder.coder.alg_rating%type
   ,p_alg_vol         in topcoder.coder.alg_vol%type
   ,p_alg_num_ratings in topcoder.coder.alg_num_ratings%type
  );

  procedure update_coder_rec(
    p_coder_id        in coder.coder_id%type
   ,p_handle          in coder.handle%type
   ,p_country_name    in coder.country_name%type
   ,p_alg_rating      in coder.alg_rating%type
   ,p_alg_vol         in coder.alg_vol%type
   ,p_alg_num_ratings in coder.alg_num_ratings%type
  );
  
  procedure fetch_coders_by_country(
    p_cursor       in out t_ref_cursor
   ,p_country_name in     topcoder.coder.country_name%type
  );

end;
/

CREATE OR REPLACE 
PACKAGE BODY topcoder.tc_maint_pkg is

  /* ======================================================================== */
  /* insert_coder_rec - insert a record to the coder table                    */
  /* ======================================================================== */
  procedure insert_coder_rec(
    p_coder_id        in topcoder.coder.coder_id%type
   ,p_handle          in topcoder.coder.handle%type
   ,p_country_name    in topcoder.coder.country_name%type
   ,p_alg_rating      in topcoder.coder.alg_rating%type
   ,p_alg_vol         in topcoder.coder.alg_vol%type
   ,p_alg_num_ratings in topcoder.coder.alg_num_ratings%type
  ) is
  begin
    insert into topcoder.coder (
      coder_id
     ,handle
     ,country_name
     ,alg_rating
     ,alg_vol
     ,alg_num_ratings
    ) values (
      p_coder_id
     ,p_handle
     ,p_country_name
     ,p_alg_rating
     ,p_alg_vol
     ,p_alg_num_ratings
    )
    ;
  end insert_coder_rec;

  /* ======================================================================== */
  /* update_coder_rec - update record in coder table                          */
  /* ======================================================================== */
  procedure update_coder_rec(
    p_coder_id        in coder.coder_id%type
   ,p_handle          in coder.handle%type
   ,p_country_name    in coder.country_name%type
   ,p_alg_rating      in coder.alg_rating%type
   ,p_alg_vol         in coder.alg_vol%type
   ,p_alg_num_ratings in coder.alg_num_ratings%type
  ) is
  begin
    update    topcoder.coder  
    set       handle          = p_handle
             ,country_name    = p_country_name
             ,alg_rating      = p_alg_rating
             ,alg_vol         = p_alg_vol
             ,alg_num_ratings = p_alg_num_ratings
    where     coder_id        = p_coder_id             
    ;
  end update_coder_rec;

  /* ======================================================================== */
  /* delete_coder_rec - delete record from coder table; note that any         */
  /*                    associated round_results records for the coder are    */
  /*                    also deleted.                                         */
  /* ======================================================================== */
  procedure delete_coder_rec(
    p_coder_id        in coder.coder_id%type
  ) is
  begin
    -- must delete associated round_results records for this coder
    -- before we can delete the coder record
    delete from topcoder.round_results
    where coder_id = p_coder_id
    ;

    -- now we can delete the coder record
    delete from topcoder.coder
    where coder_id = p_coder_id
    ;    
  end delete_coder_rec;

  /* ======================================================================== */
  /* fetch_coders_by_country - retrieve all coders for the passed in country  */
  /* ======================================================================== */
  procedure fetch_coders_by_country(
    p_cursor       in out t_ref_cursor
   ,p_country_name in     topcoder.coder.country_name%type
  ) is
  begin
    open p_cursor for
      select    c.coder_id
               ,c.handle
               ,c.alg_rating
               ,c.alg_vol
               ,c.alg_num_ratings
      from      topcoder.coder c
      where     c.country_name = p_country_name
    ;
  end fetch_coders_by_country;

end;
/

