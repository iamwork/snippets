CREATE OR REPLACE PACKAGE PG_FLOAT_COLLECTION AS

TYPE float_collection IS TABLE OF BINARY_FLOAT INDEX BY BINARY_INTEGER;

PROCEDURE test_collection (p_parm IN float_collection);
 

END PG_FLOAT_COLLECTION;
/
CREATE OR REPLACE PACKAGE BODY PG_FLOAT_COLLECTION AS

  PROCEDURE test_collection (p_parm IN float_collection) AS
  BEGIN
     FOR i IN p_parm.first .. p_parm.last
      LOOP
         dbms_output.put_line(p_parm(i));
      END LOOP;
  END test_collection;

END PG_FLOAT_COLLECTION;
/
